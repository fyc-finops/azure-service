# Azure-service

Ceci est une introduction au cours Terraform.

Plan du cours :

[1. Introduction](https://gitlab.com/fyc-finops/azure-service/-/blob/main/cours/1.%20Introduction.md?ref_type=heads)

[2. azurerm_resource_group](https://gitlab.com/fyc-finops/azure-service/-/blob/main/cours/2.%20azurerm_resource_group.md?ref_type=heads)

[3. Région-Zone](https://gitlab.com/fyc-finops/azure-service/-/blob/main/cours/3.%20R%C3%A9gion-Zone.md?ref_type=heads)

[4. azurerm_virtual_network](https://gitlab.com/fyc-finops/azure-service/-/blob/main/cours/4.%20azurerm_virtual_network.md?ref_type=heads)

[5. azurerm_network_security_group](https://gitlab.com/fyc-finops/azure-service/-/blob/main/cours/5.%20azurerm_network_security_group.md?ref_type=heads)

[6. azurerm_subnet](https://gitlab.com/fyc-finops/azure-service/-/blob/main/cours/6.%20azurerm_subnet.md?ref_type=heads)

[7. azurerm_subnet_network_security_group_association](https://gitlab.com/fyc-finops/azure-service/-/blob/main/cours/7.%20azurerm_subnet_network_security_group_association.md?ref_type=heads)

[8. azurerm_public_ip](https://gitlab.com/fyc-finops/azure-service/-/blob/main/cours/8.%20azurerm_public_ip.md?ref_type=heads)

[9. azurerm_network_interface](https://gitlab.com/fyc-finops/azure-service/-/blob/main/cours/9.%20azurerm_network_interface.md?ref_type=heads)

[10. azurerm_lb](https://gitlab.com/fyc-finops/azure-service/-/blob/main/cours/10.%20azurerm_lb.md?ref_type=heads)

[11. azurerm_virtual_machine](https://gitlab.com/fyc-finops/azure-service/-/blob/main/cours/11.%20azurerm_virtual_machine.md?ref_type=heads)

[12. azurerm_virtual_machine_scale_set](https://gitlab.com/fyc-finops/azure-service/-/blob/main/cours/12.%20azurerm_virtual_machine_scale_set.md?ref_type=heads)

[13. azurerm_private_dns_zone](https://gitlab.com/fyc-finops/azure-service/-/blob/main/cours/13.%20azurerm_private_dns_zone.md?ref_type=heads)

[14. azurerm_postgresql_flexible_server](https://gitlab.com/fyc-finops/azure-service/-/blob/main/cours/14.%20azurerm_postgresql_flexible_server.md?ref_type=heads)

[15. azurerm_postgresql_flexible_server_database](https://gitlab.com/fyc-finops/azure-service/-/blob/main/cours/15.%20azurerm_postgresql_flexible_server_database.md?ref_type=heads)
